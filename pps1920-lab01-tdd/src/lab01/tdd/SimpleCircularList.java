package lab01.tdd;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class SimpleCircularList implements CircularList {
    private List<Integer> list;
    private int current;

    public SimpleCircularList(){
        list = new LinkedList<>();
        current = 0;
    }
    @Override
    public void add(int element) {
        list.add(element);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        Optional<Integer> ret = Optional.empty();
        if (!isEmpty()) {
            ret = Optional.of(list.get(current));
            updateCurrentIndex(++current);
        }
        return ret;

    }

    @Override
    public Optional<Integer> previous() {
        Optional<Integer> ret = Optional.empty();
        if(!isEmpty()){
            ret = Optional.of(list.get(current));
            updateCurrentIndex(--current);
        }
        return ret;
    }

    @Override
    public void reset() {
        updateCurrentIndex(0);
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        Optional<Integer> ret =  Optional.empty();

        int originalCurrent = current;
        boolean appliable;
        if(!list.isEmpty()){
            do{
                ret = next();
            }
            while(!(appliable = strategy.apply(ret.get())) && current != originalCurrent);

            if(!appliable){
                ret = Optional.empty();
                current = originalCurrent;
            }
        }
        return ret;

    }

    private void updateCurrentIndex(int index){
        if(index < 0){
            current = size()-1;
        }
        else if(index == size()){
            current = 0;
        }
        else{
            current = index;
        }
    }
}
