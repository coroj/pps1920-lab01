package lab01.tdd;

public interface AbstractSelectStrategyFactory {
    SelectStrategy createEvenStrategy();
    SelectStrategy createMultipleOfStrategy(int number);
    SelectStrategy createEqualsStrategy(int number);
}
