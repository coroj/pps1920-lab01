import lab01.tdd.*;
import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {


    CircularList list;
    static AbstractSelectStrategyFactory factory;

    @BeforeAll
    static void beforeAll(){
        factory = new BasicSelectStrategyFactory();
    }

    @BeforeEach
    void beforeEach(){
        list = new SimpleCircularList();
    }

    @Test
    void initiallyEmptyList(){
        assertTrue(list.isEmpty());
    }
    @Test
    void testAddOneElement(){
        fillList(10);
        assertEquals(1,list.size());
    }

    @Test
    void testNextOnInitialList(){
        assertEquals(Optional.empty(),list.next());
    }

    @Test

    void testNextWithOneElement(){
        fillList(10);
        assertEquals(10,list.next().get());
        assertEquals(10,list.next().get());

    }

    @Test
    void testNextCircle(){
        fillList(10,20,30);
        goNextFor(2);
        assertEquals(30,list.next().get());
        assertEquals(10,list.next().get());
    }

    @Test
    void testPreviousOnInitialList(){
        assertEquals(Optional.empty(),list.previous());
    }

    @Test
    void testPrevWithOneElement(){
        fillList(10);
        assertEquals(10,list.previous().get());
        assertEquals(10,list.previous().get());

    }

    @Test
    void testPrev(){
        fillList(10,20,30);
        assertEquals(10,list.previous().get());
        assertEquals(30,list.previous().get());
        assertEquals(20,list.previous().get());
    }

    @Test
    void testReset(){
        fillList(10,20,30);
        goNextFor(1);
        assertEquals(20,list.next().get());
        list.reset();
        assertEquals(10,list.next().get());
        assertEquals(3,list.size());
    }

    @Test
    void testNextStrategyFound(){
        fillList(10,33,66,20,30);
        assertEquals(10,list.next(factory.createEvenStrategy()).get());
        goNextFor(3);
        assertEquals(33,list.next(factory.createMultipleOfStrategy(11)).get());
        assertEquals(66,list.next().get());



    }
    @Test
    void testPreviousStrategyNotFound(){
        fillList(10,33,66,20,30);
        goNextFor(4);
        assertEquals(Optional.empty(),list.next(factory.createEqualsStrategy(0)));
        assertEquals(30,list.next().get());



    }

    private void fillList(Integer... nums){
        Arrays.stream(nums).forEach(n -> list.add(n));
    }
    private void goNextFor(int rep) {
        if (rep > 0) {
            for (int i = 0; i < rep; i++) {
                list.next();
            }
        }
    }

    private void goPreviousFor(int rep) {
        if (rep > 0) {
            for (int i = 0; i < rep; i++) {
                list.previous();
            }
        }
    }


}
