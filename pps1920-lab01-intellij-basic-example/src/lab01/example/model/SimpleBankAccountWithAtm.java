package lab01.example.model;

public class SimpleBankAccountWithAtm extends SimpleBankAccount implements  BankAccountWithAtm{
    public SimpleBankAccountWithAtm(AccountHolder holder, double balance) {
        super(holder, balance);
    }

    @Override
    public void depositWithAtm(int usrID, double amount) {
        if (checkUser(usrID) && isDepositWithAtmAllowed(amount)) {
            addToBalance(amount-1);
        }
    }



    @Override
    public void withdrawWithAtm(int usrID, double amount) {
        if (checkUser(usrID) && isWithdrawWithAtmAllowed(amount)) {
            takeFromBalance(amount+1);
        }
    }

   private boolean isWithdrawWithAtmAllowed(final double amount){
       return this.getBalance() >= amount + 1;
   }

    private boolean isDepositWithAtmAllowed(double amount) {
        return amount > 1;
   }

}
