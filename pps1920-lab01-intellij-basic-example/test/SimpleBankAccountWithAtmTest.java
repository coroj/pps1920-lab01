import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;
import lab01.example.model.BankAccountWithAtm;
import lab01.example.model.SimpleBankAccountWithAtm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SimpleBankAccountWithAtmTest {

        private AccountHolder accountHolder;
        private BankAccountWithAtm bankAccount;

        @BeforeEach
        void beforeEach(){
            accountHolder = new AccountHolder("Mario", "Rossi", 1);
            bankAccount = new SimpleBankAccountWithAtm(accountHolder, 0);
        }

        @Test
        void testInitialBalance() {
            assertEquals(0, bankAccount.getBalance());
        }

        @Test
        void testDeposit() {
            bankAccount.depositWithAtm(accountHolder.getId(), 100);
            bankAccount.depositWithAtm(accountHolder.getId(), 2);
            assertEquals(100, bankAccount.getBalance());
        }

        @Test
        void testWrongDeposit() {
            bankAccount.depositWithAtm(1, 1);
            assertEquals(0, bankAccount.getBalance());
        }

        @Test
        void testWithdraw() {
            bankAccount.depositWithAtm(accountHolder.getId(), 100);
            bankAccount.withdrawWithAtm(accountHolder.getId(), 70);
            assertEquals(28, bankAccount.getBalance());
        }

        @Test
        void testWrongWithdraw() {
            bankAccount.depositWithAtm(accountHolder.getId(), 100);
            bankAccount.withdrawWithAtm(1, 100);
            assertEquals(99, bankAccount.getBalance());
        }
    }


